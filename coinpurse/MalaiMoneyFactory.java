package coinpurse;

public class MalaiMoneyFactory extends MoneyFactory {
	
	public Valuable createMoney(double value) {
		if(value < 1) {
			switch((int)(value*100)) {
			case 5 : case 10 : case 20 : case 50 :
				return new Coin(value*100, "Sen");
				
			default :
				throw new IllegalArgumentException();
			}
		}
		
		else {
			switch((int)value) {
			case 1 : case 2 : case 5 : case 10 : case 20 : case 50 : case 100 :
				return new BankNote(value, "Ringgit");
			}
		}
		throw new IllegalArgumentException();
	}
}
