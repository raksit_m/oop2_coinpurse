package coinpurse;

/**
 * An application class for testing Coin.
 * @author Raksit Mantanacharu
 */
public class CoinApp {
	/**
	 * Main class.
	 * @param args as a main class.
	 */
	public static void main (String [] args) {
		Coin one = new Coin(1, "Baht");
		AbstractValuable five = new Coin(5, "Baht");
		System.out.println(one.toString());
		System.out.println(one.compareTo(five));
		System.out.println(one.equals(five));
	}
}
