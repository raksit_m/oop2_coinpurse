package coinpurse;
/**
 * A coupon with a monetary value.
 * You can't change the value of a coupon.
 * @author Raksit Mantanacharu
 */
public class Coupon extends AbstractValuable  {	
	/** Value of the coupon. */
	private double value;
	private String color;
	enum CouponType { RED(100), BLUE(50), GREEN(20);
		private final double value;
		CouponType(double value) {
			this.value = value;
		}
	}

	/** 
	 * Constructor for a new coupon. 
	 * @param type of color determining the value of coupon.
	 */
	public Coupon(CouponType type) {
		this.value = type.value;
		this.color = type.name();
	}
	/** 
	 * Constructor for a new coupon. 
	 * @param color determining the value of coupon.
	 * if user types invalid color, throw exception.
	 */
	public Coupon(String color) {
		this.color = color.toUpperCase();
		try {
			CouponType type =CouponType.valueOf(this.color);
			this.value = type.value;
		}
		catch(Exception e) {
			throw new IllegalArgumentException("Invalid Coupon Color Name");
		}
	}
	/**
	 * The accessor method which return the value of coupon.
	 * @return Value of this coupon
	 */
	public double getValue() {
		return this.value;
	}
	/**
	 * The accessor method which return the type of coupon.
	 * @return Type of this coupon
	 */
	public String getColor() {
		return this.color;
	}
	
/**
 * The method which return the value of the coupon in term of String.
 * @return String of coupon (e.g.Red Coupon)
 */
	public String toString() {
		return this.getColor() + " Coupon";
	}
}
