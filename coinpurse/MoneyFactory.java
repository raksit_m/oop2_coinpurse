package coinpurse;

import java.util.ResourceBundle;

public abstract class MoneyFactory {
	private static MoneyFactory factory = null;

	public static MoneyFactory getInstance() {
		ResourceBundle bundle = ResourceBundle.getBundle("purse");
		String defaultFactory = bundle.getString("moneyfactory");
		if(factory == null) {
			try {
				factory = (MoneyFactory)Class.forName(defaultFactory).newInstance();
			} catch (InstantiationException e) {
				e.printStackTrace();
			}
			catch (IllegalAccessException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
		}
		return factory;
	}

	public abstract	Valuable createMoney(double value);

	public Valuable createMoney(String value) {
		double amount = Double.parseDouble(value);
		return factory.createMoney(amount);
	}
}
