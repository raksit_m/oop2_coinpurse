package coinpurse; 
/**
 * An application class for testing Purse.
 * @author Raksit Mantanacharu
 */
public class PurseTest {
	/**
	 * Main class.
	 * @param args as a main class.
	 */
	public static void main (String [] args) {
		Purse purse = new Purse( 5 ); 
		AbstractValuable coin1 = new Coin( 10, "Baht" );
		Coupon c = new Coupon("blue");
		System.out.println(purse.insert(coin1));
		System.out.println(purse.insert(c));
		System.out.println(purse.getBalance());
		System.out.println(purse.count());
		Valuable[] stuff = purse.withdraw(60);
		 // withdraw returns array of 2 items
		System.out.println(stuff[0].toString());
		System.out.println(stuff[1].toString());
		System.out.println(purse.getBalance());
	}
}