package coinpurse;

/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Raksit Mantanacharu
 */
public class Coin extends AbstractValuable {

	/** Value of the coin. */
	private double value;
	
	private String currency;

	/** 
	 * Constructor for a new coin. 
	 * @param value is the value for the coin
	 * @param currency is the currency (THB)
	 */
	public Coin( double value, String currency ) {
		this.value = value;
		this.currency = currency;
	}
	/**
	 * The accessor method which return the value of coin.
	 * @return Value of this coin
	 */
	public double getValue() {
		return this.value;
	}
	
/**
 * The method which return the value of the coin in term of String.
 * @return String of coin (e.g.5 Baht)
 */
	public String toString() {
		return (int)this.getValue() + " " + currency + " Coin";
	}
}
