package coinpurse;
/**
 * An interface for getting value from each kind of money.
 * @author Raksit Mantanacharu
 */
public interface Valuable extends Comparable<Valuable> {
/**
 * @return value of each kind of money.
 */
	public double getValue();
}
