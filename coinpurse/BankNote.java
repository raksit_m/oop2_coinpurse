package coinpurse;

/**
 * A Banknote with a monetary value.
 * You can't change the value of a Banknote.
 * @author Raksit Mantanacharu
 */
public class BankNote extends AbstractValuable {

	/** Value of the Banknote. */
	private double value;
	private static int nextSerialNumber = 1000000;
	
	private String currency;
	
	/** 
	 * Constructor for a new Banknote. 
	 * @param value is the value for the Banknote
	 */
	public BankNote( double value, String currency ) {
		this.value = value;
		this.currency = currency;
		getNextSerialNumber();
		nextSerialNumber++;
	}
	/**
	 * The accessor method which return the value of Banknote.
	 * @return Value of this Banknote
	 */
	public double getValue() {
		return this.value;
	}
	/**
	 * The accessor method which return the value of Banknote.
	 * @return Value of this Banknote
	 */
	public int getNextSerialNumber() {
		return nextSerialNumber;
	}
	
/**
 * The method which return the value of the Banknote in term of String.
 * @return String of Banknote (e.g.100 Baht Banknote)
 */
	public String toString() {
		return String.format("%d %s Banknote [%d]",(int)this.getValue(), currency, this.getNextSerialNumber());
	}
}
