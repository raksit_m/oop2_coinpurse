package coinpurse; 

import java.util.ArrayList;
import java.util.List;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A valuable money contains money.
 *  You can insert money, withdraw money, check the balance,
 *  and check if the money is full.
 *  When you withdraw money, the valuable money decides which
 *  money to remove.
 *  
 *  @author Raksit Mantanacharu
 */
public class Purse extends Observable {
	/** Collection of money in the money. */

	/** Capacity is maximum NUMBER of money the money can hold.
	 *  Capacity is set when the money is created.
	 */
	private int capacity;
	private List<Valuable> money;
	private Valuable[] valuables;
	private WithdrawStrategy strategy;

	/** 
	 *  Create a money with a specified capacity.
	 *  @param capacity is maximum number of money you can put in money.
	 */
	public Purse( int capacity ) {
		this.capacity = capacity;
		if(capacity >= 0) {
			money = new ArrayList<Valuable>();
		}
	}

	/**
	 * Change the strategy if the first one is null (could not withdraw).
	 * @param strategy (could be GreedyWithdraw or RecursiveWithdraw)
	 */
	public void setWithdrawStrategy (WithdrawStrategy strategy) {
		this.strategy = strategy;
	}

	/**
	 * Count and return the number of money in the money.
	 * This is the number of money, not their value.
	 * @return the number of money in the money
	 */
	public int count() {
		return money.size();
	}

	/** 
	 *  Get the total value of all items in the money.
	 *  @return the total value of items in the money.
	 */
	public double getBalance() {
		double balance = 0;
		for(int i = 0; i < money.size(); i++) {
			balance += money.get(i).getValue();
		}
		return balance;
	}


	/**
	 * Return the capacity of the valuable money.
	 * @return the capacity
	 */
	public int getCapacity() { 
		return this.capacity; 
	}

	/** 
	 *  Test whether the money is full.
	 *  The money is full if number of items in money equals
	 *  or greater than the money capacity.
	 *  @return true if money is full.
	 */
	public boolean isFull() {
		return this.count() == this.capacity;
	}

	/** 
	 * Insert a valuable into the money.
	 * The valuable is only inserted if the money has space for it
	 * and the valuable has positive value.  No worthless money!
	 * @param valuable is a valuable object to insert into money
	 * @return true if valuable inserted, false if can't insert
	 */
	public boolean insert( Valuable valuable ) {
		if(this.isFull() || valuable.getValue() <= 0) {
			return false;
		}
		else {
			money.add(valuable);
			super.setChanged();
			super.notifyObservers(this);
			return true;
		}
		// if the money is already full then can't insert anything.
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of money withdrawn from money,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of valuable objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		setWithdrawStrategy(new GreedyWithdraw());
		if(amount <= getBalance() && amount >= 0) {
			valuables = strategy.withdraw(amount, money);
		}
		if(valuables == null) {
			setWithdrawStrategy(new RecursiveWithdraw());
			valuables = strategy.withdraw(amount, money);
		}

		for(int i = 0; i < valuables.length; i++) {
			money.remove(valuables[i]);
		}

		//		for (Valuable valuable : tmp2) {
		//			money.remove(valuable);
		//		}
		super.setChanged();
		super.notifyObservers(this);
		return valuables;
	}

	/** 
	 * toString returns a string description of the money contents.
	 * It can return whatever is a useful description.
	 * @return String indicating amount of money and total value in Purse
	 */
	public String toString() {
		String output = "";
		output += String.format("%d money with value %.2f", money.size(), this.getBalance());
		return output;
	}
}
