/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;
/**
 * There are 2 strategies : GreedyWithdraw and RecursiveWithdraw -
 * they must have the withdraw method.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public interface WithdrawStrategy {
	
	/**
	 * 
	 * @param amount (how much the user would like to withdraw)
	 * @param valuables (as purse)
	 * @return withdrawn valuables
	 */
	Valuable[] withdraw(double amount, List<Valuable> valuables);
	
}
