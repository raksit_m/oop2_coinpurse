/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;

/**
 * Withdrawing the money by using recursion (it will call itself).
 * @author Raksit Mantanacharu 5710546402
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy {

	/** Temporary list for collecting withdrawn valuable. */
	private static List<Valuable> withdrawn = new ArrayList<Valuable>();
	/** How many times which method calls. */
	private static int times = 1;
	/** Temporary for keeping the amount (amount will be changed). */
	private static double tempAmount = 0;

	/**
	 * 1.) Sort the tempList (a copy of purse list).
	 * 2.) Then use the headWithdraw for recursive process.
	 * 3.) If the first time is failed, redo again.
	 * 4.) Convert List to Array,then return
	 * @param amount of desired withdrawn money
	 * @param valuables list as purse
	 * @return temporary list of withdrawn money
	 * 
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		tempAmount = amount;
		List<Valuable> tempList = new ArrayList<Valuable>();
		for(int i = 0; i < valuables.size(); i++) {
			tempList.add(valuables.get(i));
		}
		Collections.sort(tempList);
		List<Valuable> result = headWithdraw(tempList, tempList.size()-times, tempAmount);
		Valuable[] withdrawn = new Valuable[result.size()];
		result.toArray(withdrawn);
		return withdrawn;
	}

	/**
	 * Helper method of withdraw.
	 * 
	 * Stopping Criterion = when the size of list is zero or
	 * method check all elements of the list or withdrawal succeed.
	 * 
	 * Recursive Steps, there are 2 cases :
	 * 1.) If amount >= the first element in the list, it means that
	 * the process can still do continuously then
	 * - Amount decreases the valuables cost in the lastIndex
	 * - Add the valuable in the lastIndex to the temporary list
	 * - Recursive Step (continue checking the next element)
	 * 
	 * 2.) If amount < the first element in the list, it means that
	 * the process can't do anymore with the valuable in the temporary list
	 * but there may be another way to withdraw without that valuable
	 * - Amount increases the valuables cost of that valuable
	 * - Remove that valuable from the temporary list
	 * - Recursive Step (continue checking the next element)
	 * 
	 * For example, if we would like to withdraw 9 from [5,3,2,2]
	 * -----------------------------------------------------------------------
	 * Remaining Amount | Current Valuable | Amount >= First | Withdrawn List
	 * -----------------------------------------------------------------------
	 * 		   9		|		  5		   |	  true	     |	    [5]					
	 * -----------------------------------------------------------------------
	 * 		   4		|		  3		   |	  true		 |	   [5,3]	
	 * -----------------------------------------------------------------------
	 * 		   1        |         2        |     false		 |	    [5]
	 * -----------------------------------------------------------------------
	 * 		   4		|		  2		   |	  true		 |	   [5,2]
	 * -----------------------------------------------------------------------
	 * 		   2		|		  2		   |	  true		 |	  [5,2,2]
	 * -----------------------------------------------------------------------
	 * The result is [5,2,2]
	 * 
	 * Another example, withdraw 12 from [2,4,5,7,9]
	 * -----------------------------------------------------------------------
	 * Remaining Amount | Current Valuable | Amount >= First | Withdrawn List
	 * -----------------------------------------------------------------------
	 * 		  12		|		  9		   |	  true		 |	    [9]		
	 * -----------------------------------------------------------------------
	 * 		   3		|		  7		   |	  true		 |	   [9,7]
	 * -----------------------------------------------------------------------
	 * 		  -4		|		  5		   |	 false		 |	    [9]
	 * -----------------------------------------------------------------------
	 * 		   3		|		  5		   |	 false		 |	   [9,5]	
	 * -----------------------------------------------------------------------
	 * 		  -2		|		  4		   |	 false		 |	    [9]	      
	 * -----------------------------------------------------------------------
	 * 		   3		|		  4		   |	  true		 |	   [9,4]	 
	 * -----------------------------------------------------------------------
	 * 		  -1		|		  2		   |     false		 |	    [9]	      
	 * -----------------------------------------------------------------------
	 * 		   3		|		  2		   |	  true		 |	   [9,2]	  
	 * -----------------------------------------------------------------------     
	 * 
	 * As you can see, this is incorrect because the summation is 11 not 12.
	 * It means that we can't withdraw with 9 anymore, then we may have to do it again
	 * by removing 9 out of temporary list.
	 * 
	 * Temporary list = [2,4,5,7]
	 * -----------------------------------------------------------------------
	 * Remaining Amount | Current Valuable | Amount >= First | Withdrawn List
	 * -----------------------------------------------------------------------
	 * 		   12		|		  7		   |	  true		 |	    [7]
	 * -----------------------------------------------------------------------
	 * 		   5		|		  5		   |      true		 |	   [7,5]
	 * -----------------------------------------------------------------------
	 * 
	 * The answer is [7,5]
	 * 
	 * @param tempList for collecting the valuable which is being able to withdraw
	 * @param lastIndex	indicating the position from the last element of the list
	 * @param amount of desired withdrawn money
	 * @return temporary list of withdrawn money
	 * 
	 */
	public List<Valuable> headWithdraw(List<Valuable> tempList, int lastIndex, double amount) {
		List<Valuable> list = new ArrayList<Valuable>();
		if(tempList.size() == 0) return null;

		if(lastIndex < 0 && amount != 0) {
			withdrawn = new ArrayList<Valuable>();
			times++;
			List<Valuable> result = headWithdraw(tempList, tempList.size()-times, tempAmount);
		}

		if(lastIndex < 0 || amount == 0) {
			System.out.println("Withdrawn: " + withdrawn);
			return withdrawn;
		}

		if(amount >= tempList.get(0).getValue()) {
			withdrawn.add(0,tempList.get(lastIndex));
			System.out.println("Added Withdrawn: " + withdrawn);
			amount -= tempList.get(lastIndex).getValue();
			System.out.println("lastIndex = " + lastIndex);
			System.out.println("--------");
			list = headWithdraw(tempList, --lastIndex, amount);
		}

		else {
			amount += withdrawn.remove(0).getValue();
			System.out.println("Removed Withdrawn: " + withdrawn);
			System.out.println("lastIndex = " + lastIndex);
			System.out.println("--------");
			list = headWithdraw(tempList, lastIndex, amount);
		}
		return withdrawn;
	}
}
