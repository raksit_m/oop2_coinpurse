/*
 * This source code is Copyright 2015 by Raksit Mantanacharu
 */
package coinpurse.strategy;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import coinpurse.Valuable;

/**
 * One of the strategy which check that the most expensive valuables
 * is less than amount or not, if yes then amount decreases, otherwise do nothing.
 * @author Raksit Mantanacharu 5710546402
 *
 */
public class GreedyWithdraw implements WithdrawStrategy {
	/**
	 * Adding valuables which is being able to withdraw to the temporary list.
	 * @param amount of desired withdrawn money
	 * @param valuables list as purse
	 * @return temporary list of withdrawn money
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		//if ( ??? ) return ???;

		/*
		 * One solution is to start from the most valuable valuable
		 * in the money and take any valuable that maybe used for
		 * withdraw.
		 * Since you don't know if withdraw is going to succeed, 
		 * don't actually withdraw the money from the money yet.
		 * Instead, create a temporary list.
		 * Each time you see a valuable that you want to withdraw,
		 * add it to the temporary list and deduct the value
		 * from amount. (This is called a "Greedy Algorithm".)
		 * Or, if you don't like changing the amount parameter,
		 * use a local total to keep track of amount withdrawn so far.
		 * 
		 * If amount is reduced to zero (or tempTotal == amount), 
		 * then you are done.
		 * Now you can withdraw the money from the money.
		 * NOTE: Don't use list.removeAll(templist) for this
		 * because removeAll removes *all* money from list that
		 * are equal (using valuable.equals) to something in templist.
		 * Instead, use a loop over templist
		 * and remove money one-by-one.		
		 */


		// Did we get the full amount?
		if ( amount <= 0 ) {	
			// failed. Since you haven't actually remove
			// any money from money yet, there is nothing
			// to put back.
			return null;
		}

		// Success.
		// Since this method returns an array of valuable,
		// create an array of the correct size and copy
		// the money to withdraw into the array.
		ArrayList<Valuable> tmp1 = (ArrayList<Valuable>) valuables;
		Collections.sort(tmp1);
		ArrayList<Valuable> tmp2 = new ArrayList<Valuable>();
		for(int i = tmp1.size()-1; i >= 0; i--) {
			Valuable valuable = tmp1.get(i);
			if(amount >= valuable.getValue()) {
				tmp2.add(valuable);
				amount -= valuable.getValue();
			}
		}
		
		if(amount != 0) {
			return null;
		}
		
		Valuable[] tmp3 = new Valuable[tmp2.size()];
		tmp2.toArray(tmp3);
		return tmp3;
	}
}
