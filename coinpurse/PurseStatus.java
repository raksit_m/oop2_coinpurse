package coinpurse;

import java.awt.Panel;
import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;

/**
 * GUI which shows the status of purse and capacity progress bar.
 * This is the one of the Purse observer.
 * @author Raksit Mantanacharu
 *
 */
public class PurseStatus implements Observer {
	
	private Purse purse;
	private JLabel label;
	private JProgressBar progressBar;
	
	/**
	 * Constructor which initialize purse as a parameter and call initComponents.
	 * @param purse in Main
	 */
	public PurseStatus(Purse purse) {
		this.purse = purse;
		initComponents();
	}
	
	/**
	 * Create the components as a GUI interface.
	 */
	public void initComponents() {
		JFrame frame = new JFrame();
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		label = new JLabel("EMPTY");
		progressBar = new JProgressBar(0,purse.getCapacity());
		progressBar.setValue(0);
		progressBar.setStringPainted(true);
		panel.add(label);
		panel.add(progressBar);
		frame.add(panel);
		frame.pack();
		frame.setVisible(true);
	}

	@Override
	/**
	 * The GUI changes when purse event occurs.
	 * - If the purse is full, then show "FULL"
	 * - If the purse is empty, then show "EMPTY"
	 * - Otherwise, show the current valuables in purse.
	 * - The ProgressBar changes
	 */
	public void update(Observable o, Object arg) {
		int num = purse.count();
		if(purse.isFull()) {
			label.setText("FULL");
		}
		else if(num == 0) {
			label.setText("EMPTY");
		}
		else {
			label.setText("Capacity: " + num);
		}
		progressBar.setValue(num);
	}
}
