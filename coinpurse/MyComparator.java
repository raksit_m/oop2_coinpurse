package coinpurse;
import java.util.Comparator;
/**
 * MyComparator is used for arranging the value of each kind of money.
 * without using Coin anymore.
 * You can't change the value of a coin.
 * @author Raksit Mantanacharu
 */
public class MyComparator implements Comparator<Valuable> {
	/**
	 * compare method using the Math.signum().
	 * @param a and b, representing the kind of money.
	 * @param b and a, representing the kind of money.
	 * @return Integer(positive, zero or negative)
	 */
	public int compare(Valuable a, Valuable b) {
		double difference = (a.getValue() - b.getValue());
		return (int) Math.signum(difference);
		// compare them by value. This is easy.
	}
}
