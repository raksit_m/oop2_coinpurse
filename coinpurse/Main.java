package coinpurse; 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Raksit Mantanacharu
 */
public class Main {

	/**
	 * @param args not used
	 */
	public static void main( String[] args ) {
		Purse purse = new Purse(20);
		PurseBalance purseBalance = new PurseBalance(purse);
		purse.addObserver(purseBalance);
		PurseStatus purseStatus = new PurseStatus(purse);
		purse.addObserver(purseStatus);
		ConsoleDialog ui = new ConsoleDialog( purse );
		ui.run();
	}
}
