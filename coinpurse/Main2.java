package coinpurse;

public class Main2 {
	public static void main(String[] args) {
		MoneyFactory factory = MoneyFactory.getInstance();
		System.out.println( factory.createMoney("10") );
		System.out.println( factory.createMoney(50.0) );

//		ThaiMoneyFactory thaiFactory = new ThaiMoneyFactory();
//		Valuable m = thaiFactory.createMoney(5);
//		System.out.println(m.toString());
//		Valuable m2 = thaiFactory.createMoney("1000.0");
//		System.out.println(m2.toString());
//		Valuable m3 = thaiFactory.createMoney(0.05);
//		System.out.println(m3.toString());
//		
//		MalaiMoneyFactory malaiFactory = new MalaiMoneyFactory();
//		Valuable m4 = malaiFactory.createMoney(5);
//		System.out.println(m4.toString());
//		Valuable m5 = malaiFactory.createMoney("1000.0");
//		System.out.println(m5.toString());
//		Valuable m6 = malaiFactory.createMoney(0.05);
//		System.out.println(m6.toString());	
	}
}
