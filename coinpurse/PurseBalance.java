package coinpurse;

import java.util.Observable;
import java.util.Observer;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * GUI which shows the current balance of purse.
 * This is the one of the Purse observer.
 * @author Raksit Mantanacharu
 *
 */
public class PurseBalance implements Observer {
	private Purse purse;
	private JLabel label;

	/**
	 * Constructor which initialize purse as a parameter and call initComponents.
	 * @param purse in Main
	 */
	public PurseBalance(Purse purse) {
		this.purse = purse;
		initComponents();
	}
	
	/**
	 * Create the components as a GUI interface.
	 */
	public void initComponents() {
		JFrame frame = new JFrame();		
		label = new JLabel("Balance is " + purse.getBalance() + " Baht");
		frame.add(label);	
		frame.pack();
		frame.setVisible(true);
	}

	@Override
	/**
	 * The GUI changes when purse event occurs.
	 * - The balance changes in GUI
	 */
	public void update(Observable subject, Object info) {
		double balance = purse.getBalance();
		label.setText("Balance is " + balance + " Baht");
	}
}
