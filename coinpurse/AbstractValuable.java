package coinpurse;

/**
 * An abstract class which determine that
 * the money classes (Coin, BankNote, Coupon)
 * can use equals and compareTo without duplicated code.
 * @author Raksit Mantanacharu 
 *
 */
public abstract class AbstractValuable implements Valuable {

	/**
	 * Constructor for superclass.
	 */
	public AbstractValuable() {
		super();
	}

	/**
	 * The equals method which prove that
	 * 1.) The Object is actual Coin.
	 * 2.) The value of other coin and this coin is equal.
	 * @param arg being compared
	 * @return if 1.) and 2.) is true, then return true. Otherwise, return false.
	 */
	public boolean equals(Object arg) {
		if(arg == null) return false;
		else if(arg.getClass() != this.getClass()) return false;
		else {
			Valuable arg2 = (Valuable) arg;
			return arg2.getValue() == this.getValue();
		}
	}
	
	/**
	 * The method which compare the value of valuables :
	 * 1.) if this valuable is less than the other valuable, then return negative number.
	 * 2.) if this valuable is equal the other valuable, then return zero.
	 * 3.) if this valuable is more than the other valuable, then return positive number.
	 * @param other valuable being compared
	 * @return number depending on the condition
	 */
	public int compareTo(Valuable other) {
		int compare = 0;
		if(this.getValue() < other.getValue()) {
			compare = -1;
		}
		else if(this.getValue() == other.getValue()) {
			compare = 0;
		}
		else {
			compare = 1;
		}
		return compare;
	}
	
	
}